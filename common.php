<?php
/**
 * Created by IntelliJ IDEA.
 * User: funtik
 * Date: 013 13.01.17
 * Time: 0:50
 */

/**
 * @param string $text
 * @return Generator
 */
function lineReader($text)
{
    $text = preg_replace("/\n\s*\n/i", "\n", $text); // remove empty lines

    do {
        $pos = strpos($text, "\n");

        if (false !== $pos) {
            yield substr($text, 0, $pos);
            $text = substr($text, $pos + 1);
        } else {
            yield $text;
        }

    } while (false !== $pos);
}

/**
 * Write generated data from file (type 1)
 * @param string|resource $file
 * @return Generator
 */
function readFileLines($file)
{
    $isResource = false;

    if (is_resource($file)) {
        $handle = $file;
        $isResource = true;
    } else {
        if (!is_string($file) || !file_exists($file) || !is_file($file) || !is_readable($file)) {
            throw new \InvalidArgumentException('Invalid path to file');
        }
        $handle = fopen($file, 'rb');
    }

    $text = '';
    while (!feof($handle)) {
        $text .= fread($handle, 1024);

        $lines = iterator_to_array(lineReader($text, false));
        $linesCount = count($lines);

        foreach ($lines as $lineKey => $line) {
            // last item
            if ($lineKey === $linesCount - 1) {
                break;
            }

            // not last item
            yield $line;
        }

        $text = end($lines);

        unset($lines);
    }

    yield $text;

    if (!$isResource) {
        fclose($handle);
    }
}
