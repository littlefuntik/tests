<?php
/**
 * Created by IntelliJ IDEA.
 * User: funtik
 * Date: 013 13.01.17
 * Time: 0:45
 */

require_once dirname(__DIR__) . '/common.php';

function writeSum($filePath)
{
    $handle = fopen($filePath, 'rb+');
//    var_export([$handle, is_writable($filePath)]);die;
    flock($handle, LOCK_EX);
    $sum = 0;
    foreach (readFileLines($handle) as $line) {
        if (!is_numeric($line)) {
            throw new \Exception('Line in file do not have numeric type: ' . var_export($line, true));
        }
        $sum += $line;
    }
    fwrite($handle, "\n" . $sum);
    fflush($handle);
    flock($handle, LOCK_UN);
    fclose($handle);
}

writeSum(__DIR__ . '/test.txt');
