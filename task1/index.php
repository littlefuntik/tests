<!doctype html>
<html>
<head>
    <title>Task1 Result</title>
</head>
<body>
<table style="width:100%">
    <tr>
        <th>IP</th>
        <th>Browser</th>
        <th>OS</th>
        <th>First visit link</th>
        <th>Last visit link</th>
        <th>Unique links</th>
        <th>Time on Site</th>
    </tr>
    <?php
        $mysqli = new mysqli('localhost', 'root', '', 'tests');
        $rawSql = '
            SELECT
              s.ip_address,
              GROUP_CONCAT(DISTINCT s.browser) browser,
              GROUP_CONCAT(DISTINCT s.os) os,
              SUBSTRING_INDEX(GROUP_CONCAT(s.url_from ORDER BY s.date ASC, s.time ASC SEPARATOR ","), ",", 1) first_visit_link,
              SUBSTRING_INDEX(GROUP_CONCAT(s.url_to ORDER BY s.date DESC, s.time DESC SEPARATOR ","), ",", 1) last_visit_link,
              COUNT(DISTINCT s.url_to) unique_links_count,
              CONCAT(MAX(s.date + s.time) - MIN(s.date + s.time), "s")
            FROM stat s GROUP BY s.ip_address;
        ';
        foreach ($mysqli->query($rawSql) as $row) {
            echo '<tr><td>' . implode('</td><td>', $row) . '</td></tr>';
        }
        $mysqli->close();
    ?>
</table>
</body>
</html>