<?php

$fakeData = [
    'time' => [
        time() + 3600 * 24,
        time() + 3600 * 24 * 2,
        time() + 3600 * 12,
        time() - 3600 * 24 - 10,
        time(),
    ],
    'ip' => [
        '192.168.0.1',
        '192.168.0.2',
        '192.168.0.3',
        '192.168.0.4',
        '192.168.0.5',
        '192.168.0.6',
        '192.168.0.7',
        '192.168.0.8',
        '192.168.0.9',
        '192.168.0.20',
    ],
    'userAgent' => [
        'uagent01',
        'uagent02',
        'uagent03',
        'uagent04',
        'uagent05',
        'uagent06',
        'uagent07',
        'uagent08',
        'uagent09',
        'uagent10',
    ],
    'os' => [
        'win',
        'unix',
        'mac',
    ],
    'urlFrom' => [
        'http://google.com?from',
        'http://bing.com?from',
        'http://example.com?from',
        'http://example.to?from',
        'http://example.in?from',
        'http://example.ua?from',
    ],
    'urlTo' => [
        'http://google.com?to',
        'http://bing.com?to',
        'http://example.com?to',
        'http://example.to?to',
        'http://example.in?to',
        'http://example.ua?to',
    ],
];

/**
 * Generate fake log file for test task.
 * @param array $fakeData
 * @return array
 */
function gen1(array $fakeData) {
    $rows = [];

    for ($i = 0; $i < 1000; ++$i) {
        $timeKey = array_rand($fakeData['time'], 1);
        $rows[] = [
            'date' => date('Y-m-d', $fakeData['time'][$timeKey]),
            'time' => date('H:i:s', $fakeData['time'][$timeKey]),
            'ip' => $fakeData['ip'][array_rand($fakeData['ip'])],
            'url_from' => $fakeData['urlFrom'][array_rand($fakeData['urlFrom'])],
            'url_to' => $fakeData['urlTo'][array_rand($fakeData['urlTo'])],
        ];
    }

    return $rows;
}

/**
 * Generate fake log file for test task.
 * @param array $fakeData
 * @return array
 */
function gen2(array $fakeData) {
    $rows = [];

    for ($i = 0; $i < 1000; ++$i) {
        $rows[] = [
            'ip' => $fakeData['ip'][array_rand($fakeData['ip'])],
            'browser' => $fakeData['userAgent'][array_rand($fakeData['userAgent'])],
            'os' => $fakeData['os'][array_rand($fakeData['os'])],
        ];
    }

    return $rows;
}

$gen1 = array_map(function ($row) {
    return implode('|', $row);
}, gen1($fakeData));

$gen2 = array_map(function ($row) {
    return implode('|', $row);
}, gen2($fakeData));

file_put_contents(__DIR__ . '/1.log', implode("\n", $gen1));
file_put_contents(__DIR__ . '/2.log', implode("\n", $gen2));
