<?php
/**
 * Created by IntelliJ IDEA.
 * User: funtik
 * Date: 012 12.01.17
 * Time: 22:50
 */

require_once dirname(__DIR__) . '/common.php';

/**
 * Get structured array item from line of file log type 1
 * <br><pre>['data' => string, 'time' => string, 'ip' => string, 'url_from' => string, 'url_to' => string]</pre>
 * @param string $line
 * @return array
 */
function precessLineType1($line)
{
    if (!is_string($line)) {
        throw new InvalidArgumentException('First param must be string');
    }

    list($date, $time, $ip, $urlFrom, $urlTo) = explode('|', $line);

    return [
        'date' => $date,
        'time' => $time,
        'ip' => $ip,
        'url_from' => $urlFrom,
        'url_to' => $urlTo,
    ];
}

/**
 * Get structured array item from line of file log type 1
 * <br><pre>['ip' => string, 'browser' => string, 'os' => string]</pre>
 * @param string $line
 * @return array
 */
function precessLineType2($line)
{
    if (!is_string($line)) {
        throw new InvalidArgumentException('First param must be string');
    }

    list($ip, $browser, $os) = explode('|', $line);

    return [
        'ip' => $ip,
        'browser' => $browser,
        'os' => $os,
    ];
}

$mysqli = new mysqli('localhost', 'root', '', 'tests');

// insert items from log file type 1
$stmtInsert = $mysqli->prepare('INSERT INTO `stat` (`date`, `time`, `ip_address`, `url_from`, `url_to`) VALUES (?, ?, ?, ?, ?)');
$stmtInsert->bind_param('sssss', $date, $time, $ip, $from, $to);
foreach (readFileLines(__DIR__ . '/1.log') as $row) {

    $item = precessLineType1($row);

    $date = $item['date'];
    $time = $item['time'];
    $ip = $item['ip'];
    $from = $item['url_from'];
    $to = $item['url_to'];

    if (!$stmtInsert->execute()) {
        fwrite(STDERR, 'Error when execute query with data: ' . json_encode($item) . "\n");
    }

    unset($item, $row);
}
$stmtInsert->close();

// insert items from log file type 2
$stmtInsert = $mysqli->prepare('INSERT INTO `stat` (`ip_address`, `browser`, `os`) VALUES (?, ?, ?)');
$stmtInsert->bind_param('sss', $ip, $browser, $os);
foreach (readFileLines(__DIR__ . '/2.log') as $row) {
    $item = precessLineType2($row);

    $ip = $item['ip'];
    $os = $item['os'];
    $browser = $item['browser'];

    if (!$stmtInsert->execute()) {
        fwrite(STDERR, 'Error when execute query with data: ' . json_encode($item) . "\n");
    }

    unset($item, $row);
}
$stmtInsert->close();

$mysqli->close();

die('Bye!');